import http from 'k6/http';
import { check, sleep } from 'k6';
export let options = {
  stages: [
    { duration: '5s', target: 50 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
    { duration: '5s', target: 50 }, // stay at 100 users for 10 minutes
    { duration: '5s', target: 0 }, // ramp-down to 0 users
  ],
};
export default function () {
  let res = http.get('https://jetbro.in/');
  check(res, { 'status was 200': (r) => r.status == 200 });
  sleep(1);
}